﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Sudakov.People.Sql.EF
{
    public class PeopleContextFactory : IDesignTimeDbContextFactory<PeopleContext>
    {
        public PeopleContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PeopleContext>();
            builder.UseSqlServer(
                "Data Source=(localdb)\\mssqllocaldb;Integrated Security=SSPI;Initial Catalog=Sudakov.People");

            return new PeopleContext(builder.Options);
        }
    }
}
