﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sudakov.People.Sql.EF.Migrations
{
    public partial class AddData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Age", "FirstName", "LastName" },
                values: new object[] { 1, 10, "FirstName001", "LastName001" });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Age", "FirstName", "LastName" },
                values: new object[] { 2, 20, "FirstName002", "LastName002" });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Age", "FirstName", "LastName" },
                values: new object[] { 3, 30, "FirstName003", "LastName003" });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "PersonId", "Type" },
                values: new object[] { 1, 1, 2 });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "PersonId", "Type" },
                values: new object[] { 2, 2, 2 });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "PersonId", "Type" },
                values: new object[] { 3, 2, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
