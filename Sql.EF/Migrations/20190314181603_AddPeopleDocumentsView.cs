﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sudakov.People.Sql.EF.Migrations
{
    public partial class AddPeopleDocumentsView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"CREATE VIEW PeopleDocumentsView AS 
                SELECT p.Id, p.FirstName, p.LastName, Count(d.Id) as DocumentCount 
                FROM People p JOIN Documents d on p.Id = d.PersonId
                GROUP BY p.Id, p.FirstName, p.LastName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP VIEW PeopleDocumentsView");
        }
    }
}
