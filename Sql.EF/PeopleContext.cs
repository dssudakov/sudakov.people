﻿using Microsoft.EntityFrameworkCore;

namespace Sudakov.People.Sql.EF
{
    public class PeopleContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbQuery<PersonDocuments> PeopleDocuments { get; set; }

        public PeopleContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().HasData(
                new Person { Id = 1, FirstName = "FirstName001", LastName = "LastName001", Age = 10 },
                new Person { Id = 2, FirstName = "FirstName002", LastName = "LastName002", Age = 20 },
                new Person { Id = 3, FirstName = "FirstName003", LastName = "LastName003", Age = 30 });

            modelBuilder.Entity<Document>().HasData(
                new Document { Id = 1, PersonId = 1, Type = DocumentType.Passport },
                new Document { Id = 2, PersonId = 2, Type = DocumentType.Passport },
                new Document { Id = 3, PersonId = 2, Type = DocumentType.DriverLicense });

            modelBuilder.Query<PersonDocuments>().ToView("PeopleDocumentsView");
        }
    }
}
