﻿namespace Sudakov.People.Sql.EF
{
    public class Document
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public DocumentType Type { get; set; }
    }
}
