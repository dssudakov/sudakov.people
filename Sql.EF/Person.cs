﻿using System.Collections.Generic;

namespace Sudakov.People.Sql.EF
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public IList<Document> Documents { get; set; }
    }
}
