﻿namespace Sudakov.People.Sql.EF
{
    public enum DocumentType
    {
        DriverLicense = 1,
        Passport = 2
    }
}