﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Sudakov.People.Sql.EF.Tests
{
    [TestFixture]
    public class PeopleContextTests
    {
        private PeopleContext _context;

        [SetUp]
        public void SetUp()
        {
            _context = new PeopleContextFactory().CreateDbContext(null);
        }

        [Test]
        public async Task CanExecuteViewAsync()
        {
            var peopleDocuments = await _context.PeopleDocuments.ToListAsync();
            peopleDocuments.Should().HaveCount(2);
        }

        [Test]
        public async Task CanExecuteStoredProcedure()
        {
            var peopleDocuments = await _context.Query<PersonDocuments>().FromSql("GetPeopleDocuments").ToListAsync();
            peopleDocuments.Should().HaveCount(2);
        }
    }
}
